//
//  ArticleImageCell.swift
//  Baraka-Assignment
//
//  Created by Abdul Wahab on 07/10/2021.
//

import UIKit

class ArticleImageCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var articleImageView: UIImageView!
    static let identifier = "ArticleImageCell"

    override func awakeFromNib() {
        super.awakeFromNib()
        articleImageView.layer.cornerRadius = 12
    }
    
    func configure(titleText: String, imageUrl: String) {
        self.titleLabel.text = titleText
        setImage(imageUrl: imageUrl)
    }
    
    func setImage(imageUrl: String) {
        let imageDownloader = ImageDownloader()
        if let url = URL(string: imageUrl) {
            imageDownloader.downloadImage(from: url, success: { (image) in
                self.articleImageView.image = image
            })
        }
    }

}
