//
//  ArticleHeaderTitle.swift
//  Baraka-Assignment
//
//  Created by Abdul Wahab on 07/10/2021.
//

import UIKit

class ArticleHeaderTitle: UICollectionReusableView {
    
    @IBOutlet weak var titleLabel: UILabel!
    static let identifier = "ArticleHeaderTitle"

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(titleText: String) {
        titleLabel.text = titleText
    }
    
}
