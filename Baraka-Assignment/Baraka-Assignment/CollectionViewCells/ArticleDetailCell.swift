//
//  ArticleDetailCell.swift
//  Baraka-Assignment
//
//  Created by Abdul Wahab on 07/10/2021.
//

import UIKit

class ArticleDetailCell: UICollectionViewCell {
    
    @IBOutlet weak var articleImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    static let identifier = "ArticleDetailCell"

    override func awakeFromNib() {
        super.awakeFromNib()
        articleImageView.layer.cornerRadius = 5
    }
    
    func configure(descriptionText: String, dateText: String, imageUrl: String) {
        descriptionLabel.text = descriptionText
        dateLabel.text = dateText
        setImage(imageUrl: imageUrl)
    }
    
    func setImage(imageUrl: String) {
        let imageDownloader = ImageDownloader()
        if let url = URL(string: imageUrl) {
            imageDownloader.downloadImage(from: url, success: { (image) in
                self.articleImageView.image = image
            })
        }
    }

}
