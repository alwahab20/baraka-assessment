//
//  ViewController.swift
//  Baraka-Assignment
//
//  Created by Abdul Wahab on 06/10/2021.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func didTapOpenNewsFeed(_ sender: UIButton) {
        Router.showNewsFeed(from: self)
    }
    
}

