//
//  NewsFeedViewController.swift
//  Baraka-Assignment
//
//  Created by Abdul Wahab on 06/10/2021.
//

import UIKit

enum NewsFeedCellType: Int, CaseIterable {
    case topSection = 0
    case bottomSection = 1
}

class NewsFeedViewController: UIViewController {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var newsCollectionView: UICollectionView!
    var articles: [Articles] = []
    var viewModel: NewsFeedViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        newsCollectionView.delegate = self
        newsCollectionView.dataSource = self
        newsCollectionView.collectionViewLayout = generateLayout()
        registerCells()
        fetchNews()
    }
    
    func fetchNews() {
        activityIndicator.startAnimating()
        viewModel.showError = { (error) in
            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                let alert = UIAlertController(title: error, message: nil, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: {_ in alert.dismiss(animated: true, completion: nil)}))
                self.present(alert, animated: true, completion: nil)
            }
        }
        viewModel.newsReceived = { (news) in
            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                self.articles = news.articles ?? []
                self.newsCollectionView.reloadData()
            }
        }
        viewModel.fetchNewsFromServer()
    }
    
    func registerCells() {
        newsCollectionView.register(UINib(nibName: ArticleImageCell.identifier, bundle: nil), forCellWithReuseIdentifier: ArticleImageCell.identifier)
        newsCollectionView.register(UINib(nibName: ArticleDetailCell.identifier, bundle: nil), forCellWithReuseIdentifier: ArticleDetailCell.identifier)
        newsCollectionView.register(UINib(nibName: ArticleHeaderTitle.identifier, bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: ArticleHeaderTitle.identifier)
    }

    @IBAction func closeTapped(_ sender: UIButton) {
        Router.dismiss(from: self)
    }
}

extension NewsFeedViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case NewsFeedCellType.topSection.rawValue:
            if articles.count > 6 {
                return 6 // number of articles to display in top section
            } else {
                return 0
            }
        case NewsFeedCellType.bottomSection.rawValue:
            return articles.count
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.section {
        case NewsFeedCellType.topSection.rawValue:
            let cell = newsCollectionView.dequeueReusableCell(
                withReuseIdentifier: ArticleImageCell.identifier,
                for: indexPath) as! ArticleImageCell
            cell.configure(titleText: articles[indexPath.row].title ?? "", imageUrl: articles[indexPath.row].urlToImage ?? "")
            return cell
        case NewsFeedCellType.bottomSection.rawValue:
            let cell = newsCollectionView.dequeueReusableCell(
                withReuseIdentifier: ArticleDetailCell.identifier,
                for: indexPath) as! ArticleDetailCell
            let dateText = viewModel.getFormattedDate(from: articles[indexPath.row].publishedAt ?? "")
            cell.configure(descriptionText: articles[indexPath.row].description ?? "", dateText: dateText, imageUrl: articles[indexPath.row].urlToImage ?? "")
            return cell
        default:
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let headerView = newsCollectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: ArticleHeaderTitle.identifier, for: indexPath) as! ArticleHeaderTitle
        headerView.configure(titleText: "Section \(indexPath.section + 1)")
        return headerView
    }
    
}

extension NewsFeedViewController {
    func generateLayout() -> UICollectionViewLayout {
      let layout = UICollectionViewCompositionalLayout { (sectionIndex: Int,
        layoutEnvironment: NSCollectionLayoutEnvironment)
          -> NSCollectionLayoutSection? in
        switch sectionIndex {
        case NewsFeedCellType.topSection.rawValue:
            return self.getLayoutForFirstSection()
        case NewsFeedCellType.bottomSection.rawValue:
            return self.getLayoutForSecondSection()
        default:
            return self.getLayoutForFirstSection()
        }
      }
      return layout
    }
    
    func getLayoutForFirstSection() -> NSCollectionLayoutSection {
      let itemSize = NSCollectionLayoutSize(
        widthDimension: .fractionalWidth(1.0),
        heightDimension: .fractionalWidth(2/3))
      let item = NSCollectionLayoutItem(layoutSize: itemSize)
      let groupSize = NSCollectionLayoutSize(
        widthDimension: .fractionalWidth(0.47),
        heightDimension: .fractionalWidth(0.3))
      let group = NSCollectionLayoutGroup.horizontal(
        layoutSize: groupSize,
        subitem: item,
        count: 1)
      group.contentInsets = NSDirectionalEdgeInsets(
        top: 5,
        leading: 5,
        bottom: 5,
        trailing: 5)

      let headerSize = NSCollectionLayoutSize(
        widthDimension: .fractionalWidth(1.0),
        heightDimension: .estimated(44))
      let sectionHeader = NSCollectionLayoutBoundarySupplementaryItem(
        layoutSize: headerSize,
        elementKind: UICollectionView.elementKindSectionHeader,
        alignment: .top)

      let section = NSCollectionLayoutSection(group: group)
      section.boundarySupplementaryItems = [sectionHeader]
      section.orthogonalScrollingBehavior = .groupPaging

      return section
    }
    
    func getLayoutForSecondSection() -> NSCollectionLayoutSection {
      let itemSize = NSCollectionLayoutSize(
        widthDimension: .fractionalWidth(1.0),
        heightDimension: .fractionalWidth(1/3))
      let item = NSCollectionLayoutItem(layoutSize: itemSize)
        item.contentInsets = NSDirectionalEdgeInsets(top: 5, leading: 0, bottom: 5, trailing: 0)
      let groupSize = NSCollectionLayoutSize(
        widthDimension: .fractionalWidth(1.0),
        heightDimension: .fractionalWidth(0.5))
      let group = NSCollectionLayoutGroup.vertical(
        layoutSize: groupSize,
        subitem: item,
        count: 3)
      group.contentInsets = NSDirectionalEdgeInsets(
        top: 5,
        leading: 20,
        bottom: 5,
        trailing: 20)

      let headerSize = NSCollectionLayoutSize(
        widthDimension: .fractionalWidth(1.0),
        heightDimension: .estimated(44))
      let sectionHeader = NSCollectionLayoutBoundarySupplementaryItem(
        layoutSize: headerSize,
        elementKind: UICollectionView.elementKindSectionHeader,
        alignment: .top)

      let section = NSCollectionLayoutSection(group: group)
      section.boundarySupplementaryItems = [sectionHeader]
      section.orthogonalScrollingBehavior = .groupPaging

      return section
    }

}
