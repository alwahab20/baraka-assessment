//
//  ImageDownloader.swift
//  Baraka-Assignment
//
//  Created by Abdul Wahab on 07/10/2021.
//

import Foundation
import UIKit

class ImageDownloader {
    func downloadImage(from url: URL, success: @escaping ((UIImage) -> ())) {
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            DispatchQueue.main.async() {
                success(UIImage(data: data) ?? UIImage())
            }
        }
    }
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
}
