//
//  NetworkManager.swift
//  Baraka-Assignment
//
//  Created by Abdul Wahab on 06/10/2021.
//

import Foundation
import UIKit

class NetworkManager {
    
    func requestForNewsFeed(success: @escaping ((NewsFeedResponseModel) -> ()),
                            failure: @escaping ((String) -> ())) {
        if let url = URL(string: "https://saurav.tech/NewsAPI/everything/cnn.json") {
            var request = URLRequest(url: url)
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            let task = URLSession.shared.dataTask(with: url) { data, response, error in
                if let data = data {
                    if let news = try? JSONDecoder().decode(NewsFeedResponseModel.self, from: data) {
                        success(news)
                    } else {
                        failure("Error")
                    }
                } else if error != nil {
                    failure("Error")
                }
            }
            task.resume()
        }
    }
    
}
