//
//  Router.swift
//  Baraka-Assignment
//
//  Created by Abdul Wahab on 06/10/2021.
//

import Foundation
import UIKit

class Router {
    
    static func showNewsFeed(from currentVC: UIViewController) {
        let vc = UIStoryboard(name: "NewsFeed", bundle: Bundle.main).instantiateViewController(withIdentifier: "NewsFeedViewController") as! NewsFeedViewController
        let viewModel = NewsFeedViewModel()
        vc.viewModel = viewModel
        vc.modalPresentationStyle = .fullScreen
        currentVC.present(vc, animated: true, completion: nil)
    }
    
    static func dismiss(from currentVC: UIViewController) {
        currentVC.dismiss(animated: true, completion: nil)
    }
    
}
