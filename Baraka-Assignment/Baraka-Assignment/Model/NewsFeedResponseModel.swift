//
//  NewsFeedResponseModel.swift
//  Baraka-Assignment
//
//  Created by Abdul Wahab on 06/10/2021.
//

import Foundation

struct NewsFeedResponseModel: Decodable {
    let articles: [Articles]?
}

struct Articles: Decodable {
    let title: String?
    let urlToImage: String?
    let description: String?
    let publishedAt: String?
}
