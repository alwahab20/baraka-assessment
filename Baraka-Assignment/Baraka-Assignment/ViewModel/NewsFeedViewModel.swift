//
//  NewsFeedViewModel.swift
//  Baraka-Assignment
//
//  Created by Abdul Wahab on 06/10/2021.
//

import Foundation

public class NewsFeedViewModel: NSObject {
    let networkManager: NetworkManager
    
    private(set) var errorMessage : String! {
        didSet {
            self.showError?(errorMessage)
        }
    }
    
    var showError : ((String) -> ())?
    var newsReceived : ((NewsFeedResponseModel) -> ())?

    public override init() {
        self.networkManager = NetworkManager()
        super.init()
    }
    
    func getFormattedDate(from string: String) -> String {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:SSZ"
        if let date = formatter.date(from: string) {
            formatter.dateFormat = "MMM-dd"
            return formatter.string(from: date)
        } else {
            return ""
        }
    }
    
    func fetchNewsFromServer() {
        networkManager.requestForNewsFeed(success: { (news) in
            self.newsReceived?(news)
        }, failure: { (error) in
            self.showError?(error)
        })
    }
}

